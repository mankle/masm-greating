@echo off

    if exist "p2.obj" del "p2.obj"
    if exist "p2.exe" del "p2.exe"

    \masm32\bin\ml /c /coff "p2.asm"
    if errorlevel 1 goto errasm

    \masm32\bin\PoLink /SUBSYSTEM:CONSOLE "p2.obj"
    if errorlevel 1 goto errlink
    dir "p2.*"
    goto TheEnd

  :errlink
    echo _
    echo Link error
    goto TheEnd

  :errasm
    echo _
    echo Assembly Error
    goto TheEnd
    
  :TheEnd

pause
