include \masm32\include\masm32rt.inc  ; includes all masm library

    .data
        question db 'What is your name?: ', 0 ; char array of question
        greating db 'Hello ', 0   ; char array of greating

    .data?
        answer db 100 dup(?)  ; char buffer to store the name 

    .code

start:
    invoke StdOut, addr question      ; call system output with question address 
    invoke StdIn, addr answer, 100    ; call system input
    invoke StdOut, addr greating      ; call system output with greating address 
    invoke StdOut, addr answer        ; call system output with answer address 
    inkey NULL                        ; wait for key input
    exit                              ; exit program

end start
